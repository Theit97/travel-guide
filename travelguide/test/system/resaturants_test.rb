require "application_system_test_case"

class ResaturantsTest < ApplicationSystemTestCase
  setup do
    @resaturant = resaturants(:one)
  end

  test "visiting the index" do
    visit resaturants_url
    assert_selector "h1", text: "Resaturants"
  end

  test "creating a Resaturant" do
    visit resaturants_url
    click_on "New Resaturant"

    fill_in "Contact", with: @resaturant.contact
    fill_in "Location", with: @resaturant.location
    fill_in "Name", with: @resaturant.name
    click_on "Create Resaturant"

    assert_text "Resaturant was successfully created"
    click_on "Back"
  end

  test "updating a Resaturant" do
    visit resaturants_url
    click_on "Edit", match: :first

    fill_in "Contact", with: @resaturant.contact
    fill_in "Location", with: @resaturant.location
    fill_in "Name", with: @resaturant.name
    click_on "Update Resaturant"

    assert_text "Resaturant was successfully updated"
    click_on "Back"
  end

  test "destroying a Resaturant" do
    visit resaturants_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Resaturant was successfully destroyed"
  end
end
