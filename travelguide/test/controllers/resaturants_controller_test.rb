require 'test_helper'

class ResaturantsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @resaturant = resaturants(:one)
  end

  test "should get index" do
    get resaturants_url
    assert_response :success
  end

  test "should get new" do
    get new_resaturant_url
    assert_response :success
  end

  test "should create resaturant" do
    assert_difference('Resaturant.count') do
      post resaturants_url, params: { resaturant: { contact: @resaturant.contact, location: @resaturant.location, name: @resaturant.name } }
    end

    assert_redirected_to resaturant_url(Resaturant.last)
  end

  test "should show resaturant" do
    get resaturant_url(@resaturant)
    assert_response :success
  end

  test "should get edit" do
    get edit_resaturant_url(@resaturant)
    assert_response :success
  end

  test "should update resaturant" do
    patch resaturant_url(@resaturant), params: { resaturant: { contact: @resaturant.contact, location: @resaturant.location, name: @resaturant.name } }
    assert_redirected_to resaturant_url(@resaturant)
  end

  test "should destroy resaturant" do
    assert_difference('Resaturant.count', -1) do
      delete resaturant_url(@resaturant)
    end

    assert_redirected_to resaturants_url
  end
end
