Rails.application.routes.draw do
  resources :homes
  resources :tours
  resources :flights
  resources :resaturants
  resources :guesthouses
  resources :hotels
  resources :places
  root "places#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
