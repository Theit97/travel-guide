json.extract! resaturant, :id, :name, :location, :contact, :created_at, :updated_at
json.url resaturant_url(resaturant, format: :json)
