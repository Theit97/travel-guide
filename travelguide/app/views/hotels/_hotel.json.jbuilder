json.extract! hotel, :id, :name, :location, :contact, :created_at, :updated_at
json.url hotel_url(hotel, format: :json)
