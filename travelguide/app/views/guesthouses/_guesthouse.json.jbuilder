json.extract! guesthouse, :id, :name, :location, :contact, :created_at, :updated_at
json.url guesthouse_url(guesthouse, format: :json)
