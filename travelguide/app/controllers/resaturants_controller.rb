class ResaturantsController < ApplicationController
  http_basic_authenticate_with name: "Resturant", password: "hero1234", except: [:index, :show]
  
  before_action :set_resaturant, only: [:show, :edit, :update, :destroy]

  # GET /resaturants
  # GET /resaturants.json
  def index
    @resaturants = Resaturant.all
  end

  # GET /resaturants/1
  # GET /resaturants/1.json
  def show
  end

  # GET /resaturants/new
  def new
    @resaturant = Resaturant.new
  end

  # GET /resaturants/1/edit
  def edit
  end

  # POST /resaturants
  # POST /resaturants.json
  def create
    @resaturant = Resaturant.new(resaturant_params)

    respond_to do |format|
      if @resaturant.save
        format.html { redirect_to @resaturant, notice: 'Resaturant was successfully created.' }
        format.json { render :show, status: :created, location: @resaturant }
      else
        format.html { render :new }
        format.json { render json: @resaturant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /resaturants/1
  # PATCH/PUT /resaturants/1.json
  def update
    respond_to do |format|
      if @resaturant.update(resaturant_params)
        format.html { redirect_to @resaturant, notice: 'Resaturant was successfully updated.' }
        format.json { render :show, status: :ok, location: @resaturant }
      else
        format.html { render :edit }
        format.json { render json: @resaturant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /resaturants/1
  # DELETE /resaturants/1.json
  def destroy
    @resaturant.destroy
    respond_to do |format|
      format.html { redirect_to resaturants_url, notice: 'Resaturant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resaturant
      @resaturant = Resaturant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resaturant_params
      params.require(:resaturant).permit(:name, :location, :contact)
    end
end
