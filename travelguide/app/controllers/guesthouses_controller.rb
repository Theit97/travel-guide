class GuesthousesController < ApplicationController
  http_basic_authenticate_with name: "Place", password: "hero1234", except: [:index, :show]
  before_action :set_guesthouse, only: [:show, :edit, :update, :destroy]

  # GET /guesthouses
  # GET /guesthouses.json
  def index
    @guesthouses = Guesthouse.all
  end

  # GET /guesthouses/1
  # GET /guesthouses/1.json
  def show
  end

  # GET /guesthouses/new
  def new
    @guesthouse = Guesthouse.new
  end

  # GET /guesthouses/1/edit
  def edit
  end

  # POST /guesthouses
  # POST /guesthouses.json
  def create
    @guesthouse = Guesthouse.new(guesthouse_params)

    respond_to do |format|
      if @guesthouse.save
        format.html { redirect_to @guesthouse, notice: 'Guesthouse was successfully created.' }
        format.json { render :show, status: :created, location: @guesthouse }
      else
        format.html { render :new }
        format.json { render json: @guesthouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /guesthouses/1
  # PATCH/PUT /guesthouses/1.json
  def update
    respond_to do |format|
      if @guesthouse.update(guesthouse_params)
        format.html { redirect_to @guesthouse, notice: 'Guesthouse was successfully updated.' }
        format.json { render :show, status: :ok, location: @guesthouse }
      else
        format.html { render :edit }
        format.json { render json: @guesthouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /guesthouses/1
  # DELETE /guesthouses/1.json
  def destroy
    @guesthouse.destroy
    respond_to do |format|
      format.html { redirect_to guesthouses_url, notice: 'Guesthouse was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_guesthouse
      @guesthouse = Guesthouse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def guesthouse_params
      params.require(:guesthouse).permit(:name, :location, :contact)
    end
end
